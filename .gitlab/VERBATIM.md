# VERBATIM.

`Model : 2023-04-13-self.`

| param | value |
| ------ | ------ |
| **license** | Choose license from [https://spdx.org/licenses/](https://spdx.org/licenses/). |
| **tokens** | Create private token `GITLAB_TOKEN` with rights `api`, `write_repository`, `write_registry`. |
| **CI/CD variables** | Add `CUSTOM_EMAIL` (Protected, Masked, Expanded), `CUSTOM_NAME` (Protected, Expanded) and `GITLAB_TOKEN` (Protected, Masked, Expanded). |
| **CI/CD files** | Create _./.gitlab/default-ci.yml_ & _./.gitlab/extend-ci.yml_, based on '_Self_' home, and _./gitlab-ci.yml_. |
| **Composer** | Add [packagist.org](https://packagist.org/) connection in '_parameters > integrations_'. |
| **pipeline schedule** | x |
| **pipeline trigger** | Action "web" (eq "build" keyword trigger) or "build" keywords in commit. |